﻿#include <iostream>
#include "math.h"

using namespace std;


class Vector
{

private:
    double x = 0;
    double y = 0;
    double z = 0;
    double value = sqrt((x * x) + (y * y) + (z * z));

public:
    
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        cout << x << ' ' << y << ' ' << z;
        cout << "\n" << value << "\n";
    }

    operator double()const { return value; }
    

};

int main()
{
    Vector v(2, 2, 2);
    v.Show();
    return 0;
}